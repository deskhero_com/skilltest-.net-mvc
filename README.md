# ASP.NET MVC5 (or newer) / Code first - skill test #

The overall goals with this task is to download and launch a prepared project with two basic pages called “suppliers” and “products”, and then extend the project with a “category” page (with parent/child relations) using https://www.jstree.com

If you are reading this, you have most likely been invited to perform this skill test and we will value the following things in the evaluation:
* Code quality.
* Code performance (avoid unnecessary db requests etc).
* Use the same code structure as used in existing project.



### Pre-requirements ###

**Tools needed**

* Visual Studio 2017 or newer
* SQL Server 2017 (express) or newer


**Setup**

* Download/Clone the project
* Launch in Visual Studio
* Restore nuget packages
* Make sure the project “WebApp” is set as “start up project”
* Set "default project" in package manager console to "Data"
* Modify connection string in WebbApp\web.config to use your sql server instance name (and authorization)
* Exec the “update-database” command in package manager console to create the DB and generate the initial tables and data

![Get started](https://bitbucket.org/jimmiea/skilltest-.net-mvc/raw/bb96d16b8fb300ce39591b8e6e23111265f9a888/Readme/startup.png)

* Running the WebApp (F5) should now launch the web app

![Start up project](https://bitbucket.org/jimmiea/skilltest-.net-mvc/raw/bb96d16b8fb300ce39591b8e6e23111265f9a888/Readme/products.png)




### Task overview ###

* 1 Create categories (tree) view
* 2 Add categories to product (optional task)


### Task1: Create categories ###

* Create data class with the properties you need to fulfill this task and the appropriate foreign keys.  Migrate to database (Code first migrations).
* Add ViewModel, Controller and View to manage the categories.
* Add to top menu

Use http://www.jstree.com plugin

It should look something like this:

![Categories view](https://bitbucket.org/jimmiea/skilltest-.net-mvc/raw/bb96d16b8fb300ce39591b8e6e23111265f9a888/Readme/categories.jpg)

**Functionality**

* Add a root called ”Categories”
* Allow drag n drop
* Add context (right click) menu with Create, Rename and Delete.

![Context menu](https://bitbucket.org/jimmiea/skilltest-.net-mvc/raw/bb96d16b8fb300ce39591b8e6e23111265f9a888/Readme/context-menu.jpg)

**Functionality in buttons**

* **Add folder** button adds a folder to the root (or current selected folder if any is selected)
* **Add category** button adds a category to the selected folder (if none, throw message saying “You must select folder”)
* **Save changes** button posts the entire tree with ajax to the controller and saves to database. Throw message saying “Changes saved”
* **Cancel** button redirects user back to the page (to reload)

***Note: Only save categories when “Save changes” is clicked. Do NOT call server and save every time a folder is added/moved/renamed or removed***




### Task 2. Add categories to product (OPTIONAL) ###

Now we are going to use the categories we created by letting every product choose one or more.

Extend the existing Product objects (View/Controller/ViewModel etc) with Categories

It should look something like this:

![Categories on product view](https://bitbucket.org/jimmiea/skilltest-.net-mvc/raw/bb96d16b8fb300ce39591b8e6e23111265f9a888/Readme/products-extended.jpg)


### DONE ###

Zip or rar your project and send it to the contact that contacted you about the test.