﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels.Products
{
    public class ProductsVM
    {
        public Data.Models.Product Product { get; set; }
        public IEnumerable<Data.Models.Product> Products { get; set; }
    }
}