﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InititalMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        SupplierId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Supplier", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.Supplier",
                c => new
                    {
                        SupplierId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SupplierId);

            AddSampleData();
        }


        private void AddSampleData()
        {
            

            Sql($"insert into Supplier (Name, CreatedDate) values ('Supplier1', GetUtcDate())");
            Sql($"insert into Supplier (Name, CreatedDate) values ('Supplier2', GetUtcDate())");
            Sql($"insert into Supplier (Name, CreatedDate) values ('Supplier3', GetUtcDate())");

            Sql($"insert into Product (Name, CreatedDate, SupplierId) values ('Product1', GetUtcDate(), 1)");
            Sql($"insert into Product (Name, CreatedDate, SupplierId) values ('Product2', GetUtcDate(), 2)");
            Sql($"insert into Product (Name, CreatedDate, SupplierId) values ('Product3', GetUtcDate(), 2)");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Product", "SupplierId", "dbo.Supplier");
            DropIndex("dbo.Product", new[] { "SupplierId" });
            DropTable("dbo.Supplier");
            DropTable("dbo.Product");
        }
    }
}
